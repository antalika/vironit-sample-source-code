/**
 * Copyright (c) 2017, Vironit and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is part of VironIT software; Part of Relief app.
 *
 * PrefLoader.cs -  part of main app
 *
 * @author   VironIT
 * @version  12.1
 * @since    1.0
 *
 */


using UnityEngine;
using System;
using System.Collections;
using System.IO;


namespace UnityFramework.PrefabsManagement
{
  /**
   * PrefabLoader main class
   * @type {MonoBehaviour}
   */
	public class PrefabLoader : MonoBehaviour 
	{
		/**
		 *	Create - create new Game object
		 * 
		 * @return loader
		 */
		public static PrefabLoader Create()
		{
			var go = new GameObject("PrefabLoader");
			GameObject.DontDestroyOnLoad(go);

			var loader = go.AddComponent<PrefabLoader>();
			return loader;
		}


		private enum State { Idle, Loading, Completed }

		private State _currentState = State.Idle;
		private string _path = null;
		private ResourceRequest _request = null;
		private Action<UnityEngine.GameObject> _completed = null;
		private GameObject _prefab = null;

		/**
		 * Load - load Game object
		 *
		 * @return {GameObject}
		 */
		public GameObject Load(string path)
		{
			if (_currentState != State.Idle)
				throw new InvalidOperationException();

			_currentState = State.Loading;
			_path = path;

			GameObject.Destroy(gameObject);
			_prefab = (GameObject)Resources.Load(_path, typeof(GameObject));

			_currentState = State.Completed;

			return _prefab;
		}

		/**
		 * LoadAsync - async laod Game object
		 */
		public void LoadAsync(string path, Action<UnityEngine.GameObject> completed)
		{
			if (_currentState != State.Idle)
				throw new InvalidOperationException();

			_currentState = State.Loading;
			_path = path;
			_completed = completed;

			_request = Resources.LoadAsync(path, typeof(GameObject));
		}

		/**
		 * Standart Update method
		 */
		void Update()
		{
			if (_currentState == State.Loading)
			{
				if (_request.isDone)
				{
					_currentState = State.Completed;

					GameObject.Destroy(gameObject);

					_prefab = (GameObject)_request.asset;
					_completed(_prefab);
				}
			}
		}

		/**
		 * Standart Upload method
		 */
		public void Unload()
		{
			if (_prefab != null)
			{
				Resources.UnloadAsset(_prefab);
				_prefab = null;

				Resources.UnloadUnusedAssets();
			}
		}
	}
}

